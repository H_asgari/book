from django.contrib.auth.hashers import make_password
from django.utils import timezone
from rest_framework import serializers
from books import models, exceptions
from django.contrib.auth.models import User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class WriterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Writer
        fields = [
            'id', 'name', 'age', 'address'
        ]


class BookSerializer(serializers.ModelSerializer):
    writer = WriterSerializer(read_only=True)
    writer_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = models.BookModel
        fields = [
            'id', 'name', 'description', 'writer', 'is_read', 'is_finish', 'writer_id', 'rate', 'image_file'
        ]
        image_file = serializers.ImageField(use_url=True)


class BookRateSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(write_only=True)
    book_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = models.BookRate
        fields = [
            'user_id', 'book_id', 'rate'
        ]

    def create(self, validated_data):
        if not User.objects.filter(id=validated_data['user_id']).exists():
            raise exceptions.UserDoesNotExists

        if not models.BookModel.objects.filter(id=validated_data['book_id']).exists():
            raise exceptions.BookDoesNotExists

        return super().create(validated_data)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = 'id', 'username', 'password'

    def create(self, validated_data, ):
        password = make_password(validated_data['password'])
        validated_data['password'] = password
        return super().create(validated_data)


class BookAvgSerializer(serializers.Serializer):
    average_of_book = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.BookRate
        fields = [
            'book_id'
        ]


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField(write_only=True)


class LogoutSerializer(serializers.Serializer):
    pass


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    default_error_messages = {
        'no_active_account': 'کاربر فعالی با اطلاعات وارد شده یافت نشد.'
    }

    @classmethod
    def get_token(cls, user):
        user.last_login = timezone.now()
        user.save()
        token = super().get_token(user)
        token['is_superuser'] = user.is_superuser
        token['username'] = user.username
        return token
