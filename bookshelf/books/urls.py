from django.urls import path

from books import views

from books.views import CustomTokenObtainPairView

urlpatterns = [
    path('list/', views.BookListCreate.as_view()),
    path('item/<int:book_id>/', views.BookItems.as_view()),

    path('writer/list/', views.WriterListCreate.as_view()),
    path('writer/<int:writer_id>/', views.WriterItems.as_view()),

    path('rate/books/', views.BookRateCreate.as_view()),
    path('users/', views.UserList.as_view()),
    path('users/create', views.UserCreate.as_view()),
    path('user/<int:user_id>', views.UserItem.as_view()),
    path('user/logout', views.LogOutApiView.as_view()),
    path('user/login/', CustomTokenObtainPairView.as_view()),

    path('avg/<int:book_id>', views.AverageRatingBooks.as_view())

]
