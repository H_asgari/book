from django.contrib import admin
from books import models

# Register your models here.
admin.site.register(models.BookModel)
admin.site.register(models.Writer)
admin.site.register(models.BookRate)
