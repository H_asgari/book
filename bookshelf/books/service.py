

class Service:
    @classmethod
    def filter_query_params(cls, query_params):
        kwargs = {}
        for key, value in query_params.items():
            kwargs[key] = value
        return cls.model.objects.filter(**kwargs)
