very_bad = 1
bad = 2
normal = 3
good = 4
very_good = 5
rate_choices = (
    (very_bad, 'خیلی بد'),
    (bad, 'بد'),
    (normal, 'متوسط'),
    (good, 'خوب'),
    (very_good, ' خیلی خوب')
)
