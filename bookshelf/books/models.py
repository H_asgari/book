from django.db import models
from .consts import rating


class BookModel(models.Model):
    name = models.CharField(verbose_name='نام کتاب', max_length=100)
    description = models.CharField(max_length=100)
    writer = models.CharField(max_length=100)
    image_file = models.FileField(upload_to='media')
    is_read = models.BooleanField(null=True, blank=True)

    is_finish = models.BooleanField(null=True, blank=True)
    writer = models.ForeignKey('writer', verbose_name='نویسنده', on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class Writer(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    address = models.CharField(max_length=500)

    def __str__(self):
        return self.name


class BookRate(models.Model):
    book = models.ForeignKey('BookModel', on_delete=models.PROTECT)
    user = models.ForeignKey('auth.user', on_delete=models.PROTECT)
    rate = models.IntegerField(choices=rating.rate_choices)




