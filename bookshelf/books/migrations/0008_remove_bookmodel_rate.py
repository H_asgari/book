# Generated by Django 3.2.11 on 2023-01-31 20:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0007_bookmodel_rate'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookmodel',
            name='rate',
        ),
    ]
