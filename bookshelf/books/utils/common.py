import re
import datetime
import jdatetime
import importlib.util

from urllib.parse import urlencode, urlparse, urlunparse, parse_qs
from django.utils import timezone
from django.utils.timezone import localtime


def camel_to_snake(name):
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()


def en2fa(string):
    string = str(string)
    dict_digit = {'0': '۰', '1': '۱', '2': '۲', '3': '۳', '4': '۴', '5': '۵', '6': '۶', '7': '۷', '8': '۸', '9': '۹'}

    for digit in dict_digit:
        string = string.replace(digit, dict_digit[digit])

    return string


def fa2en(string):
    dict_digit = {'۰': '0', '۱': '1', '۲': '2', '۳': '3', '۴': '4', '۵': '5', '۶': '6', '۷': '7', '۸': '8', '۹': '9'}

    for digit in dict_digit:
        string = string.replace(digit, dict_digit[digit])

    return string


def jalali_date(value):
    if type(value) not in [datetime.date, datetime.datetime.date]:
        return value

    j = jdatetime.GregorianToJalali(gday=value.day, gmonth=value.month, gyear=value.year)
    return '{year}-{month:02d}-{day:02d}'.format(year=j.jyear, month=j.jmonth, day=j.jday)


def jalali_datetime(date):
    if type(date) != datetime.datetime:
        return date

    date = localtime(date)
    j = jdatetime.GregorianToJalali(gday=date.day, gmonth=date.month, gyear=date.year)
    return '{year}-{month:02d}-{day:02d} {hour:02d}:{minute:02d}'.format(
        year=j.jyear, month=j.jmonth, day=j.jday, hour=date.hour, minute=date.minute
    )


def jalali_datetime_fa(date):
    if type(date) != datetime.datetime:
        return date

    date = localtime(date)
    j = jdatetime.GregorianToJalali(gday=date.day, gmonth=date.month, gyear=date.year)
    return en2fa(
        '{hour:02d}:{minute:02d} {year}/{month:02d}/{day:02d}'.format(
            year=j.jyear, month=j.jmonth, day=j.jday, hour=date.hour, minute=date.minute
        )
    )


def add_now_to_filename(filename):
    date = localtime(timezone.now())
    j = jdatetime.GregorianToJalali(gday=date.day, gmonth=date.month, gyear=date.year)
    return '{filename}_{year}-{month:02d}-{day:02d}_{hour:02d}-{minute:02d}'.format(
        filename=filename, year=j.jyear, month=j.jmonth, day=j.jday, hour=date.hour, minute=date.minute
    )


def is_int(s):
    """Return 's is blank or represents an int'"""
    if not s:
        return True
    try:
        int(s)
        return True
    except ValueError:
        return False


def merge_dict(first_dict, second_dict):
    return {**first_dict, **second_dict}


def remove_parameter_from_url(url, params=None):
    if params is None:
        params = []

    u = urlparse(url)
    query = parse_qs(u.query, keep_blank_values=True)

    # add report settings to params
    for key in query.keys():
        if key.startswith('rs_'):
            params.append(key)

    # remove specified parameters
    for param in params:
        query.pop(param, None)
        u = u._replace(query=urlencode(query, True))

    return urlunparse(u)


def get_file_extension(filename):
    return filename.split('.')[-1] if '.' in filename else None
