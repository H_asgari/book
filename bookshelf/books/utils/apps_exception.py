from books.utils.common import camel_to_snake


class AppException(Exception):
    code = 'error'
    message = 'خطای سمت سرور به وجود آمد'
    status = 400
    field_pattern = False

    def __init__(self, field_pattern=False):
        self.field_pattern = field_pattern
        app = self.__class__.__module__.split('.')[1]
        self.code = app + '_' + camel_to_snake(type(self).__name__)

    def __str__(self):
        return '{}: {}'.format(self.code, self.message)


class AppExceptionList(Exception):
    exceptions = []
    status = 400

    def __init__(self, exceptions=None):
        if exceptions is None:
            exceptions = []
        self.exceptions = exceptions


class AppExceptionDict(Exception):
    exceptions = dict()
    status = 400

    def __init__(self, exceptions=None) -> None:
        if exceptions is None:
            exceptions = dict()
        self.exceptions = exceptions

    def get_json_representation(self) -> dict:
        def get_dict_value(obj):
            if type(obj) == dict:
                data = dict()
                for key, value in obj.items():
                    data[key] = get_dict_value(value)
                return data

            elif type(obj) == list:
                messages = []
                for item in obj:
                    messages.append(get_dict_value(item))
                return messages

            elif isinstance(obj, Exception):
                return obj.message

            return obj

        response_dict = dict()
        for key, value in self.exceptions.items():
            response_dict[key] = get_dict_value(value)

        return response_dict


class ObjectNotFound(AppException):
    def __init__(self, model):
        self.message = "%s مورد نظر یافت نشد." % model._meta.verbose_name
        app = self.args[0].__module__.split('.')[1]
        self.code = "%s_%s_not_found" % (app, camel_to_snake(model._meta.object_name))
        self.status = 404
        self.field_name = model._meta.object_name.lower() + '_id'


class ObjectNotActive(AppException):
    # TODO: این باید چک شود
    def __init__(self, model):
        self.message = "%s مورد نظر فعال نمی باشد." % model._meta.verbose_name
        app = self.args[0].__module__.split('.')[1]
        self.code = "%s_%s_not_active" % (app, camel_to_snake(model._meta.object_name))
        self.status = 404
        self.field_name = model._meta.object_name.lower() + '_id'


class DbIntegrityError(AppException):
    def __init__(self, model):
        self.message = "به دلیل حفظ جامعیت بانک اطلاعاتی امکان انجام این عمل وجود ندارد."
        self.code = "%s_integrity_error" % model._meta.object_name.lower()
        self.status = 400
        self.field_name = model._meta.object_name.lower() + '_id'


class DbRollbackError(AppException):
    def __init__(self, model):
        self.message = "با توجه به خطای به وجود آمده، تغییرات انجام شده برگردانده شد."
        self.code = "%s_rollback_error" % model._meta.object_name.lower()
        self.status = 400
        self.field_name = model._meta.object_name.lower() + '_id'


class DataTypeError(AppException):
    def __init__(self, field_name):
        self.message = f" فرمت  فیلد {field_name} نادرست می‌باشد."
        self.code = "%s_data_type_error" % field_name.lower()
        self.status = 400
