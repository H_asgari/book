from books.utils.apps_exception import AppException


class UserDoesNotExists(AppException):
    message = 'کابر مورد نظر یافت نشد.'


class BookDoesNotExists(AppException):
    message = 'کتاب مورد نظر یافت نشد.'


class BookListValueNotValid(AppException):
    message = 'مقدار وراد شده اشتباه است.'
