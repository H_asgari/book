from django.contrib.auth import authenticate, login, logout
from books import models

from django.contrib.auth.models import User
# Create your views here.
from rest_framework import generics, status, permissions
from books import serializers, exceptions
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from books import Service

from books.serializers import LoginSerializer

from books.serializers import LogoutSerializer
from rest_framework_simplejwt.views import TokenObtainPairView


class BookListCreate(generics.ListCreateAPIView, Service):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.BookSerializer
    model = models.BookModel

    def get_queryset(self):
        return self.filter_query_params(self.request.query_params)


class BookItems(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.BookSerializer

    def get_object(self):
        return models.BookModel.objects.get(id=self.kwargs['book_id'])


class WriterListCreate(generics.ListCreateAPIView, Service):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.WriterSerializer
    model = models.Writer

    def get_queryset(self):
        return self.filter_query_params(self.request.query_params)


class WriterItems(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.WriterSerializer

    def get_object(self):
        return models.Writer.objects.get(id=self.kwargs['writer_id'])


class BookRateCreate(generics.CreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.BookRateSerializer


class UserList(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.UserSerializer

    def get_queryset(self):
        return User.objects.all()


class UserCreate(generics.CreateAPIView):
    serializer_class = serializers.UserSerializer


class UserItem(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.UserSerializer

    def get_object(self):
        queryset = User.objects.all()
        if not queryset.filter(id=self.kwargs['user_id']).exists():
            raise exceptions.UserDoesNotExists
        return queryset.get(id=self.kwargs['user_id'])


class AverageRatingBooks(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.BookAvgSerializer

    def get(self, request, *args, **kwargs):
        queryset = models.BookRate.objects.filter(book_id=self.kwargs['book_id'])
        rates = []
        for item in queryset:
            rates.append(item.rate)
        avg = sum(rates) / len(rates)
        serializer = self.get_serializer({'average_of_book': avg})
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LoginAPIView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = authenticate(request, username=serializer.data['username'], password=self.request.data['password'])
        if user is not None:
            login(request, user)
            return Response({'message': 'Login successful'})
        return Response({'message': 'Invalid username or password'})


class LogOutApiView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = LogoutSerializer

    def post(self, request):
        logout(request)
        return Response({'detail': 'logout successfully'})


class CustomTokenObtainPairView(TokenObtainPairView):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = serializers.CustomTokenObtainPairSerializer
